<?php

namespace Training\Registration\Controller\Customer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class index extends Action
{
    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {

        $this->_view->loadLayout();
        $this->_view->renderLayout();

    }

}
