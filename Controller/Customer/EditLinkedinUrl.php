<?php

namespace Training\Registration\Controller\Customer;

use Magento\Customer\Block\Account\Dashboard\Info;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InputMismatchException;

class EditLinkedinUrl extends Action
{

    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @param Context $context
     * @param CustomerRepository $customerRepository
     */
    public function __construct(Context $context, CustomerRepository $customerRepository)
    {
        parent::__construct($context);
        $this->customerRepository = $customerRepository;
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws InputMismatchException
     */
    public function execute()
    {
        /** @var Info $block */

        // Get an instance of the session to get Customer Id
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $om->get('Magento\Customer\Model\Session');
        $customerData = $customerSession->getCustomer()->getData();

        $customerId = $customerData["entity_id"];
        $linkedinProfile = $_POST["linkedin_profile"] ?? " Url not valid ";
        $customer = $this->customerRepository->getById($customerId);
        $customer->setCustomAttribute("linkedin_profile", $linkedinProfile);

        $_SESSION["linkedin_profile"] = $linkedinProfile;
        $this->customerRepository->save($customer);

        $resultRedirect = $this->resultRedirectFactory->create();
        $customerBeforeAuthUrl = $this->_url->getUrl('customer/account/');
        return $resultRedirect->setPath($customerBeforeAuthUrl);


    }
}

